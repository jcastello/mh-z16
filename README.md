# MH-Z16

Python Library for reading CO2 data from the MH-Z16 Infrared CO2 sensor (0-5000ppm) with UART breakout.

DFROBOT SKU SEN0220 [https://wiki.dfrobot.com/Infrared_CO2_Sensor_0-50000ppm_SKU__SEN0220](https://wiki.dfrobot.com/Infrared_CO2_Sensor_0-50000ppm_SKU__SEN0220)
  

